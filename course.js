let params = new URLSearchParams(window.location.search);

let courseId = params.get("courseId");

fetch(`http://localhost:4000/courses/${courseId}`)
  .then((res) => res.json())
  .then((data) => {
    console.log(data);
  });
